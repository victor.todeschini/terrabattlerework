using System.Collections.Generic;
using UnityEngine;


namespace Data
{
    [CreateAssetMenu(fileName = "New Rarity Database", menuName = "Rarity Database")]
    public class RarityDatabase : ScriptableObject
    {
        public List<RarityData> allRarities;
    }
}