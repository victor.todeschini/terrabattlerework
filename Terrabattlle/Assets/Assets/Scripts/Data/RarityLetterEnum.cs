
namespace Data
{
    public enum RarityLetterEnum
    {
        E,
        D,
        C,
        B,
        A,
        S,
        SS,
        Z
    }
}
