using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;


namespace Data
{
    [CreateAssetMenu(fileName = "ObtainedCharacterCollectionDataBase", menuName = "ObtainedCharacterCollectionDataBase")]


    public class ObtainedCharacterCollectionDataBase : ScriptableObject
    {
        public List<CharacterData> ObtainedCharacter = new List<CharacterData>();

        private string characterDataPath;

        private void Awake()
        {
            characterDataPath = Path.Combine(Application.persistentDataPath, "character_data.json");
        }

        public void AddObtainedCharacter(CharacterData character)
        {
            ObtainedCharacter.Add(character);
        }


        // usefull only for testing, delete the entire collection
        public void ClearObtainedCharacters()
        {
            ObtainedCharacter.Clear();
        }


    }

}