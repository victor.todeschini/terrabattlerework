using UnityEngine;

namespace Data
{
    [System.Serializable]
    public class RarityData
    {
        public string rarity;

        public float appearanceChance;

         //Calculate the appearance chance as a decimal value (0 to 1) from the percentage.
        public float GetAppearanceChance()
        {
           return appearanceChance / 100f;
        }
    }

}