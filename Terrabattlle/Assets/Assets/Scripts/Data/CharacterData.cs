using UnityEngine;


namespace Data
{
    [CreateAssetMenu(fileName = "New Character Data", menuName = "Character Data")]


    public class CharacterData : ScriptableObject
    {

    public RarityLetterEnum rarityLetter;
    public string characterName;
    public int level;
    public int attack;
    public int defense;
    public Sprite icon;
    public Sprite splashArt;
    // Add more character attributes as needed.
    }

}