using System.Collections.Generic;
using UnityEngine;


namespace Data
{
    [CreateAssetMenu(fileName = "New Character Database", menuName = "Character Database")]
    public class CharacterDatabase : ScriptableObject
    {
        public List<CharacterData> allCharacters;
    }
}
