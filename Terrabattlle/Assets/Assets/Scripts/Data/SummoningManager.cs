using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.IO;
using Data;

namespace Data
{
    public class SummoningManager : MonoBehaviour
    {
        [SerializeField]
        private CharacterDatabase characterDatabase;

        [SerializeField]
        private RarityDatabase rarityDatabase;

        [SerializeField]
        private ObtainedCharacterCollectionDataBase obtainedCharactersDatabase;

        [SerializeField]
        private Image splashArtImage;

        [SerializeField]
        private GameObject displayPanel;

        [SerializeField]
        private Button nextButton;

        [SerializeField]
        private GameObject summonUi;

        [SerializeField]
        private TMP_Text rarityText;

        [SerializeField]
        private TMP_Text characterNameText;

        private string characterDataPath;
        private List<CharacterData> displayedCharacters = new List<CharacterData>();
        private int currentIndex = 0;

        private void Awake()
        {
            characterDataPath = Path.Combine(Application.persistentDataPath, "character_data.json");
            LoadCharacterData();
            rarityText = rarityText.GetComponent<TMP_Text>();
            characterNameText = characterNameText.GetComponent<TMP_Text>();
            CloseDisplayPanel();
        }

        public void PerformSingleGachaPull()
        {
            PerformGachaPull(1);
        }

        public void PerformTenGachaPull()
        {
            PerformGachaPull(10);
        }

        private void PerformGachaPull(int numberOfPulls)
        {
            float totalChance = 0f;
            foreach (RarityData rarityData in rarityDatabase.allRarities)
            {
                totalChance += rarityData.appearanceChance;
            }

            List<CharacterData> obtainedCharacters = new List<CharacterData>();

            for (int i = 0; i < numberOfPulls; i++)
            {
                float randomValue = Random.Range(0f, totalChance);
                string selectedRarity = "";

                foreach (RarityData rarityData in rarityDatabase.allRarities)
                {
                    randomValue -= rarityData.appearanceChance;
                    if (randomValue <= 0f)
                    {
                        selectedRarity = rarityData.rarity;
                        break;
                    }
                }

                List<CharacterData> filteredCharacters = new List<CharacterData>();
                foreach (CharacterData character in characterDatabase.allCharacters)
                {
                    if (character.rarityLetter.ToString() == selectedRarity)
                    {
                        filteredCharacters.Add(character);
                    }
                }

                if (filteredCharacters.Count == 0)
                {
                    filteredCharacters = characterDatabase.allCharacters;
                }

                CharacterData randomCharacter = filteredCharacters[Random.Range(0, filteredCharacters.Count)];
                obtainedCharacters.Add(randomCharacter);
            }

            obtainedCharactersDatabase.ObtainedCharacter.AddRange(obtainedCharacters);
            SaveCharacterData();

            displayedCharacters = obtainedCharacters;
            currentIndex = 0;
            ShowNextCharacter();
            HideSummonButtons();
            OpenDisplayPanel();
        }

        private void ShowNextCharacter()
        {
            if (currentIndex < displayedCharacters.Count)
            {
                CharacterData character = displayedCharacters[currentIndex];
                splashArtImage.sprite = character.splashArt;
                rarityText.text = character.rarityLetter.ToString();
                characterNameText.text = character.characterName;
                currentIndex++;

            }
            else
            {
                CloseDisplayPanel();
                ShowSummonButtons();
            }
        }

        public void OnNextButtonPressed()
        {
            ShowNextCharacter();
        }

        private void CloseDisplayPanel()
        {
            displayPanel.SetActive(false);
        }

        private void OpenDisplayPanel()
        {
            displayPanel.SetActive(true);
        }

        private void ShowSummonButtons()
        {
            summonUi.gameObject.SetActive(true);
        }

        private void HideSummonButtons()
        {
            summonUi.gameObject.SetActive(false);
        }

        private void SaveCharacterData()
        {
            string characterDataJson = JsonUtility.ToJson(obtainedCharactersDatabase);
            File.WriteAllText(characterDataPath, characterDataJson);
        }

        private void LoadCharacterData()
        {
            if (File.Exists(characterDataPath))
            {
                string characterDataJson = File.ReadAllText(characterDataPath);
                JsonUtility.FromJsonOverwrite(characterDataJson, obtainedCharactersDatabase);
            }
            else
            {
                obtainedCharactersDatabase.ClearObtainedCharacters();
            }
        }
    }
}
