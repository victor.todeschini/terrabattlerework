using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Data
{
    public class CollectionManager : MonoBehaviour
    {
        [SerializeField]
        private ObtainedCharacterCollectionDataBase obtainedCharactersDatabase;

        [SerializeField]
        private Transform gridLayoutParent;

        [SerializeField]
        private GameObject characterPrefab;

        [SerializeField]
        private GameObject collectionPanel;

        [SerializeField]
        private GameObject characterDataPanel;

        [SerializeField]
        private TextMeshProUGUI characterDPName;

        [SerializeField]
        private TextMeshProUGUI characterDPRarity;

        [SerializeField]
        private Image characterDPSplashArt;

        [SerializeField]
        private Image characterDPIconRank1;


        public void SpawnCharacterObjects()
        {
            ClearCharacterObjects();
            foreach (CharacterData character in obtainedCharactersDatabase.ObtainedCharacter)
            {
                GameObject characterObject = Instantiate(characterPrefab, gridLayoutParent);
                Image iconImage = characterObject.GetComponent<Image>();


                // assign characterdata to button script
                characterObject.GetComponent<CharacterButton>().characterData = character;
                characterObject.GetComponent<CharacterButton>().collectionPanel = collectionPanel;
                characterObject.GetComponent<CharacterButton>().characterDataPanel = characterDataPanel;
                characterObject.GetComponent<CharacterButton>().characterDPName = characterDPName;
                characterObject.GetComponent<CharacterButton>().characterDPRarity = characterDPRarity;
                characterObject.GetComponent<CharacterButton>().characterDPSplashArt = characterDPSplashArt;
                characterObject.GetComponent<CharacterButton>().characterDPIconRank1 = characterDPIconRank1;

                if (iconImage != null && character.icon != null)
                {
                    iconImage.sprite = character.icon; // Assign the icon sprite
                }
            }
        }

        public void ClearCharacterObjects()
        {
            int childCount = gridLayoutParent.childCount;

            for (int i = childCount - 1; i >= 0; i--)
            {
                GameObject child = gridLayoutParent.GetChild(i).gameObject;
                Destroy(child);
            }
        }
    }
}
