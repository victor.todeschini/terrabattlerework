using UnityEngine;
using TMPro;
using UnityEngine.UI;

namespace Data
{
    public class CharacterButton : MonoBehaviour
    {
        public CharacterData characterData;

        public GameObject collectionPanel;

        public GameObject characterDataPanel;

        public TextMeshProUGUI characterDPName;

        public TextMeshProUGUI characterDPRarity;

        public Image characterDPSplashArt;

        public Image characterDPIconRank1;

        public void OpenCharacterInfo()
        {
            characterDPSplashArt.sprite = characterData.splashArt;
            characterDPIconRank1.sprite = characterData.icon;
            characterDPName.text = characterData.name;
            characterDPRarity.text = characterData.rarityLetter.ToString();
            characterDataPanel.gameObject.SetActive(true);
            collectionPanel.gameObject.SetActive(false);
         
        }
    }
}
